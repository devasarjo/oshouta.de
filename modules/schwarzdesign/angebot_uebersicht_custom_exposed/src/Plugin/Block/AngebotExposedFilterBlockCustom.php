<?php

namespace Drupal\angebot_uebersicht_custom_exposed\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 * Provides a 'Angebot Exposed Filter Custom' Block.
 *
 * @Block(
 *   id = "angebot_uebersicht_custom_exposed",
 *   admin_label = @Translation("Angebot Exposed Filter Block"),
 * )
 */
class AngebotExposedFilterBlockCustom extends BlockBase {

  /**
   * {@inheritdoc}
   */

  public function build() {

    $build = array();
  	$build['#cache']['max-age'] = 0;

    $angebot_exposed_content = $this->generateExposedAngebote();
    $build['#filter'] = $angebot_exposed_content;
    $build['#theme'] = 'angebot_uebersicht_custom_exposed';

    return $build;

  }

  /**
   * Angebote Exposwed Filter
   *
   * @return array
   */
  private function generateExposedAngebote() {

    $my_filter = "";
    // Hier ggf umdefinieren: Tägliche Meditationen, Kurzmassagen, Einzelsitzungen
    $daily_med_type = 18;
    $kurzmassagenkalender_type = 45;


    // Taxonomy Vokabular für Themen
    $tax_themen = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('angebotsthema', $parent = 0, $max_depth = TRUE, $load_entities = FALSE);
    $thema_selected = ' selected';
    $select_markup_themen = '';
    foreach($tax_themen as $term){
      //print_r($term->name);
      //print_r($term->tid);
      $select_markup_themen .= '<option value="'.$term->tid.'"';
      if(isset($_GET['thema']) && !empty($_GET['thema'])){
        if($term->tid == $_GET['thema']){
          $select_markup_themen .= ' selected';
          $thema_selected = '';
        }
      }
      $select_markup_themen .= '>'.$term->name.'</option>';
    }

     // Taxonomy Vokabular für Typen
     $tax_typen = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('angebotstyp', $parent = 0, $max_depth = TRUE, $load_entities = FALSE);
     $select_markup_typen = '';
     $typ_selected = ' selected';
     foreach($tax_typen as $term){
       if($term->tid != $daily_med_type AND $term->tid != $kurzmassagenkalender_type) {
       $select_markup_typen .= '<option value="'.$term->tid.'"';
       if(isset($_GET['typ']) && !empty($_GET['typ'])){
         if($term->tid == $_GET['typ']){
           $select_markup_typen .= ' selected';
           $typ_selected = '';
         }
       }
       $select_markup_typen .= '>'.$term->name.'</option>';
     }
     }

     //Dozenten
     $ids = \Drupal::entityQuery('user')
     ->condition('status', 1)
     ->condition('roles', 'dozent')
     ->sort('field_name', 'ASC')
     ->execute();

     //filter für dozenten, denen keine angebote zugeordnet sind
     foreach($ids as $k => $id){
       $references = \Drupal::entityQuery('node')
       ->condition('type', 'angebot')
       ->condition('field_dozenten', $id)
       ->count()
       ->execute();
       if(intval($references) === 0) unset($ids[$k]);
     }


     $users = User::loadMultiple($ids);
     $usernames = '';
     $username_selected = ' selected';
     foreach($users as $user){
      $u_name = $user->get('field_name')->value;
      $usernames .= '<option value="' . $u_name . '"';
      if(isset($_GET['dozent']) && !empty($_GET['dozent'])){
        if($u_name == $_GET['dozent']){
          $usernames .= ' selected';
          $username_selected = '';
        }
      }
      $usernames .= '>' . $u_name;
      $usernames .= '</option>';
     }


     $now = new \DateTime('now', new \DateTimezone('UTC'));
     $option_date = new \DateTime('now', new \DateTimezone('UTC'));
     $current_year = intval($now->format('Y'));
     $tmp_year = intval($current_year);
     $current_month = intval($now->format('m'));
     $tmp_month = intval($current_month + 1);

     $months_german = [
       'Jan' => 'Januar',
       'Feb' => 'Februar',
       'Mar' => 'März',
       'Apr' => 'April',
       'May' => 'Mai',
       'Jun' => 'Juni',
       'Jul' => 'Juli',
       'Aug' => 'August',
       'Sep' => 'September',
       'Oct' => 'Oktober',
       'Nov' => 'November',
       'Dec' => 'Dezember'];

     $date_select = '';
     $date_selected = ' selected';
     for($i = 1; $i < 12; $i++){

       if($tmp_month == 13)
       {
         $tmp_year = intval($tmp_year) + 1;
         $tmp_month = intval(01);
       }

       $option_date->setDate($tmp_year, $tmp_month, 1);
       $date_select .= '<option value="' . $option_date->format('Y-m-d') . '"';
       if(isset($_GET['datum']) && !empty($_GET['datum'])){
         if($option_date->format('Y-m-d') == $_GET['datum']){
           $date_select .= ' selected';
           $date_selected = '';
         }
       }
       $date_select .= '>Ab ' . $months_german[$option_date->format('M')] . ' ' .$option_date->format('Y');
       $date_select .= '</option>';

       $tmp_month ++;
     }

     $my_filter = $this->t('
      <div class="bg_grey">
      <div class="container main-content" id="exposed-filter">
      <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <h1 class="programm-exposed-headline">Programmkalender</h1>
      </div>
      <form method="get">
       <input type="hidden" name="page" value="0">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="form-item">
          <div class="select-wrapper full-width">
            <select class="form-control" name="typ" size="1">
            <option value="All" '.$typ_selected.'>Alle Typen</option>
              '.$select_markup_typen.'
            </select>
          </div>
        </div>
        <div class="form-item">
          <div class="select-wrapper full-width">
            <select class="form-control" name="thema" size="1">
            <option value="All" '.$thema_selected.'>Alle Themen</option>
              '.$select_markup_themen.'
            </select>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="form-item">
          <div class="select-wrapper full-width">
            <select class="form-control" name="dozent">
              <option value="" '.$username_selected.'>Alle Dozenten</option>
              '.$usernames.'
            </select>
          </div>
        </div>
        <div class="form-item">
          <div class="select-wrapper full-width">
            <select class="form-control" name="datum">
              <option value="" '.$date_selected.'>Ab aktuellem Datum</option>
              '.$date_select.'
            </select>
          </div>
        </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <div class="form-item pull-right">
          <button class="btn btn-default btn-programm-submit" type="submit">Anzeigen</button>
        </div>
      </div>
      </form>
      </div>
      </div>
      </div>
      ');

// $my_filter = "AAA";
      return $my_filter;
  }

}
