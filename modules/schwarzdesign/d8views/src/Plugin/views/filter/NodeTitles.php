<?php

  /**
   * @file
   * Definition of Drupal\d8views\Plugin\views\filter\NodeTitles.
   */

  namespace Drupal\d8views\Plugin\views\filter;

  use Drupal\views\Plugin\views\filter\FilterPluginBase;
  use Drupal\views\Plugin\views\filter\InOperator;
  use Drupal\views\Plugin\views\filter\ManyToOne;
  use Drupal\views\ViewExecutable;
  use Drupal\views\Views;
  /**
   * Filters by given list of node title options.
   *
   * @ingroup views_filter_handlers
   *
   * @ViewsFilter("d8views_node_titles")
   */
  class NodeTitles extends FilterPluginBase {
      // exposed filter options
      protected $alwaysMultiple = TRUE;

      /**
       * Provide simple equality operator
       */
      public function operatorOptions() {
          return [
              'online' => $this->t('Online'),
              'upcoming' => $this->t('Upcoming'),
              'upcoming_online' => $this->t('Upcoming und Online'),
              'archive' => $this->t('Vergangene'),
          ];
      }


      public function query() {
          //Get the current domain.
          //$domain = domain_get_domain();
          $nu_in_utc = new \DateTime('now', new \DateTimezone('UTC'));
          $nu_in_utc_in_iso = $nu_in_utc->format('Y-m-d\TH:i:s');
          $nu_date = $nu_in_utc->format('Y-m-d');
          /*
          * Voeg relatie met datum veiling toe
          */
          $configuration = [
              'table'      => 'node__field_datum',
              'left_table' => 'node_field_data',
              'left_field' => 'nid',
              'field'      => 'entity_id',
              'type'       => 'LEFT',
              'extra_operator'   => 'AND',
          ];
          $join = Views::pluginManager('join')->createInstance('standard', $configuration);
          $this->query->addRelationship('node__field_datum', $join, 'node_field_data');

          switch($this->operator) {
              case 'online':
                  /*
                  * Condition 'online'
                  */
                  $this->query->addWhere('AND', 'node__field_datum.field_datum_end_value', $nu_in_utc_in_iso, '>');
                  $this->query->addWhere('AND', 'node__field_datum.field_datum_value', $nu_date, '<=');
                  break;
              case 'upcoming':
                  /*
                  * Condition 'Upcoming'
                  */
                  $this->query->addWhere('AND', 'node__field_datum.field_datum_end_value', $nu_in_utc_in_iso, '>');
                  $this->query->addWhere('AND', 'node__field_datum.field_datum_value', $nu_date, '>');
                  break;

              case 'upcoming_online':
                  /*
                  * Condition 'Upcoming'
                  */
                  $this->query->addWhere('AND', 'node__field_datum.field_datum_end_value', $nu_in_utc_in_iso, '>');
                  break;

              case 'archive':
                  /*
                  * Condition 'Upcoming'
                  */
                  $this->query->addWhere('AND', 'node__field_datum.field_datum_end_value', $nu_in_utc_in_iso, '<');
                  break;

          }
      }
  }
