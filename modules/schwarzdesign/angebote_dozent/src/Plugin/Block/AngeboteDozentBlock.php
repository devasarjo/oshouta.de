<?php

/**
 * @file
 // * Contains \Drupal\angebote_dozent\Plugin\Block\AngboteDozentBlock.
 */

namespace Drupal\angebote_dozent\Plugin\Block;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatch;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;

/**
 * Provides the AngeboteDozentBlock.
 *
 * @Block(
 *   id="angebote_dozent",
 *   admin_label = @Translation("Angebote Dozent"),
 * )
 */
class AngeboteDozentBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

	$build = array();
	$build['#cache']['max-age'] = 0;

  $datum = date('Y-m-d');

  $path = \Drupal::request()->getPathInfo();
  $path_args = explode('/', $path);

  if((isset($path_args[1]) and $path_args[1] == 'profile') and (isset($path_args[2]) and is_numeric($path_args[2]))){
    $dozent = $path_args[2];
  }
  elseif((isset($path_args[2]) and $path_args[2] == 'profile') and (isset($path_args[3]) and is_numeric($path_args[3]))){
    $dozent = $path_args[3];
  }
  else {
    $dozent = 0;
  }



    $angebot_content = $this->generateAngeboteDozent($datum, $dozent);
    $build['#data'] = $angebot_content;

		$build['#theme'] = 'angebote_dozent';


    return $build;
  }

  /**
 * Success Story teaser
 *
 * @return array
 */
private function generateAngeboteDozent($a_datum, $a_dozent) {

  $my_results = array();


$result = array();
$my_results = array();
$counter = 0;
//echo "<br />a_datum: " . $a_datum;


$language = \Drupal::languageManager()->getCurrentLanguage();

$current_langcode = $language->getId();


if($a_dozent != "") {
  $dozent_join = "LEFT JOIN node__field_dozenten as doz ON (doz.entity_id = value.node_id)";
  $dozent_join .= "LEFT JOIN profile as profid ON (doz.field_dozenten_target_id = profid.uid and profid.type = 'dozent')";
  $dozent_string = "AND profid.profile_id = ".$a_dozent."";
}
else {
  $dozent_join = "";
  $dozent_string = "";
}

$result = db_query("SELECT SQL_CALC_FOUND_ROWS DISTINCT value.node_id as 'Node_ID', value.date_start as 'Start_Date', value.date_end as 'End_Date', value.nur_anfang, value.termintyp, value.keine_uhrzeit

FROM
(
  (SELECT n.nid as node_id, fdu.field_datum_uhrzeit_value as date_start, fdu.field_datum_uhrzeit_end_value as date_end, az.field_nur_anfangszeit_anzeigen_value as nur_anfang, 0 as termintyp, ku.field_keine_uhrzeit_anzeigen_value as keine_uhrzeit
  FROM node__field_datum_uhrzeit as fdu
  INNER JOIN node__field_termin as ft
  ON (ft.field_termin_target_id = fdu.entity_id)
  INNER JOIN node_field_data as n
  ON (n.nid = ft.entity_id AND n.status = 1)
  LEFT JOIN node__field_nur_anfangszeit_anzeigen as az
  ON(az.entity_id = ft.field_termin_target_id AND az.deleted = 0)
  LEFT JOIN node__field_keine_uhrzeit_anzeigen as ku
  ON(ku.entity_id = ft.field_termin_target_id AND ku.deleted = 0)
  )
  UNION
(
  SELECT n.nid as node_id, ftr.field_termin_regel_value_raw as date_start, ftr.field_termin_regel_end_value_raw as date_end, NULL as nur_anfang, 1 as termintyp, NULL as keine_uhrzeit
  FROM date_recur__node__field_termin_regel as ftr
  INNER JOIN node_field_data as n
  ON (n.nid = ftr.entity_id AND n.status = 1)
  INNER JOIN node__field_termin_regel as tr
  ON (tr.entity_id = ftr.entity_id AND tr.deleted = 0)
)
) value


LEFT JOIN node__field_thema as thema
ON (thema.entity_id = value.node_id)
LEFT JOIN node__field_typ as typ
ON (typ.entity_id = value.node_id)
" . $dozent_join. "

WHERE 1
".$dozent_string. "
AND value.date_start >= '".$a_datum."'
ORDER BY value.date_start
LIMIT 3;
");




//print_r($result->fetchAssoc());
while($row = $result->fetchAssoc()){


  $entity_manager = \Drupal::entityTypeManager();
  $node = $entity_manager->getStorage('node')->load($row['Node_ID']);


  $tmp_title = $node->get('title')->value;
  $tmp_path_alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$row['Node_ID']);


  $tmp_start = $row['Start_Date'];
  $tmp_ende = $row['End_Date'];
  $tmp_keine_uhrzeit = $row['keine_uhrzeit'];
  $tmp_nur_anfang = $row['nur_anfang'];
  if($tmp_nur_anfang == "") {
    $tmp_nur_anfang = 0;
  }
  // echo "<br />tmp_nur_anfang: " . $tmp_nur_anfang;

date_default_timezone_set('UTC');
$tmp_start_timestamp = strtotime($tmp_start);
$tmp_ende_timestamp = strtotime($tmp_ende);

$tmp_date_formatted = $this->formatterTerminString($tmp_start_timestamp,$tmp_ende_timestamp,$tmp_nur_anfang,$row['termintyp'],$tmp_keine_uhrzeit);


  $my_results[$counter]['nid'] = $row['Node_ID'];
  $my_results[$counter]['zeitraum'] = $tmp_date_formatted;
  $my_results[$counter]['title'] = $tmp_title;
  $my_results[$counter]['path_alias'] = $tmp_path_alias;


  $counter ++;
}

return $my_results;

}


/**
 * Formatter for Termin Dates
 *
 * @return string  DateString
 */

 private function formatterTerminString($utc_termin_start_timestamp,$utc_termin_ende_timestamp,$nur_anfang,$termintyp,$keine_uhrzeit) {

   // echo "<br />utc_termin_start_timestamp: " . $utc_termin_start_timestamp;
   // echo "<br />utc_termin_ende_timestamp: " . $utc_termin_ende_timestamp;


  $date_formatted = '';


  if($termintyp == 1) {
    date_default_timezone_set('UTC');
    $start_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_days');
    $ende_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_days');
  }
  else {
    date_default_timezone_set('Europe/Berlin');
    $start_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_days');
    $ende_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_days');
  }

  if($start_formatted_short != $ende_formatted_short) {
    $date_formatted = $start_formatted_short . ' - ' . $ende_formatted_short;
  }
  else {


    if($nur_anfang == 1) {
      $start_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_time');

      if($keine_uhrzeit == 1) {
        $date_formatted = $start_formatted_short;
      } else {
        $date_formatted = $start_formatted_short . ', ab ' . $start_formatted_time . ' Uhr';
      }
    }
    else {
      $start_formatted = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_medium_date_days');
      $ende_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_time');

      // echo "<br />start_formatted: " . $start_formatted;
      // echo "<br />ende_formatted_time: " . $ende_formatted_time;


      if($keine_uhrzeit == 1) {
        $date_formatted = $start_formatted_short . ' - ' . $ende_formatted_short;
      }
      else {
        $date_formatted = $start_formatted . ' - ' . $ende_formatted_time . ' Uhr';
      }
    }


  }


  return $date_formatted;
}

}
