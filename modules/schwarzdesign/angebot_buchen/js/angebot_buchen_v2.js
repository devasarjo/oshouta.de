(function($) {
$(document).ready(function() {

  var input_fields = $(".form-item-kontoinhaber, .form-item-iban, .form-item-bic, .form-item-name-der-bank");
//  var input_labels = $(".form-item-kontoinhaber label, .form-item-iban label, .form-item-bic label, .form-item-name-der-bank label");
  var input_labels_de = $(".form-item-kontoinhaber label, .form-item-iban label");
  var input_labels_ausland = $(".form-item-bic label, .form-item-name-der-bank label");
//  var inputs = $('#edit-kontoinhaber, #edit-iban, #edit-bic, #edit-name-der-bank');  
  var inputs_de = $('#edit-kontoinhaber, #edit-iban');
  var inputs_ausland = $('#edit-bic, #edit-name-der-bank');
  
  input_fields.hide();
  $(".form-item-iban-valid").hide();

  $("#edit-zahlungsweise").on('change', function() {
     //console.log("change to value: " + this.value );
    if(this.value >= 3) {
      input_fields.hide();
      $(".form-item-iban-valid").hide();
      input_labels_de.removeClass('form-required');
      input_labels_ausland.removeClass('form-required');
      inputs_de.attr('required', false);
      inputs_ausland.attr('required', false);
    } else {
      input_fields.show();
      input_labels_de.addClass('form-required');
      inputs_de.attr('required', true);
      $("#edit-iban").blur();
    }
  })

  $.validator.addMethod("not_none", function(value) {
    //console.log('Zahlungsweise val: ' + $("#edit-zahlungsweise").val());
    if($("#edit-zahlungsweise").val() < 3) {
	   return 0;
    } else {
	   return 1;
    }
  });

  /**
 * IBAN is the international bank account number.
 * It has a country - specific format, that is checked here too
 *
 * Validation is case-insensitive. Please make sure to normalize input yourself.
 */
$("#edit-iban").on('blur', function() {

  // Remove spaces and to upper case
  var iban = $(this).val().replace( / /g, "" ).toUpperCase(),
    ibancheckdigits = "",
    leadingZeroes = true,
    is_iban_valid = true,
    cRest = "",
    cOperator = "",
    error_string = "",
    countrycode, ibancheck, charAt, cChar, bbanpattern, bbancountrypatterns, ibanregexp, i, p;
  // if iban is empty we don't do anything, just quitting
  if ( !iban.trim() ) {
    return;
  }
  // Check for IBAN code length.
  // It contains:
  // country code ISO 3166-1 - two letters,
  // two check digits,
  // Basic Bank Account Number (BBAN) - up to 30 chars
  // $("#edit-iban-korrekt").empty();
  var minimalIBANlength = 5;
  if ( iban.length < minimalIBANlength ) {
    error_string = "IBAN zu kurz";
    is_iban_valid = false;
  } else {
    // Check the country code and find the country specific format
    countrycode = iban.substring( 0, 2 );
    bbancountrypatterns = {
      "AL": "\\d{8}[\\dA-Z]{16}",
      "AD": "\\d{8}[\\dA-Z]{12}",
      "AT": "\\d{16}",
      "AZ": "[\\dA-Z]{4}\\d{20}",
      "BE": "\\d{12}",
      "BH": "[A-Z]{4}[\\dA-Z]{14}",
      "BA": "\\d{16}",
      "BR": "\\d{23}[A-Z][\\dA-Z]",
      "BG": "[A-Z]{4}\\d{6}[\\dA-Z]{8}",
      "CR": "\\d{17}",
      "HR": "\\d{17}",
      "CY": "\\d{8}[\\dA-Z]{16}",
      "CZ": "\\d{20}",
      "DK": "\\d{14}",
      "DO": "[A-Z]{4}\\d{20}",
      "EE": "\\d{16}",
      "FO": "\\d{14}",
      "FI": "\\d{14}",
      "FR": "\\d{10}[\\dA-Z]{11}\\d{2}",
      "GE": "[\\dA-Z]{2}\\d{16}",
      "DE": "\\d{18}",
      "GI": "[A-Z]{4}[\\dA-Z]{15}",
      "GR": "\\d{7}[\\dA-Z]{16}",
      "GL": "\\d{14}",
      "GT": "[\\dA-Z]{4}[\\dA-Z]{20}",
      "HU": "\\d{24}",
      "IS": "\\d{22}",
      "IE": "[\\dA-Z]{4}\\d{14}",
      "IL": "\\d{19}",
      "IT": "[A-Z]\\d{10}[\\dA-Z]{12}",
      "KZ": "\\d{3}[\\dA-Z]{13}",
      "KW": "[A-Z]{4}[\\dA-Z]{22}",
      "LV": "[A-Z]{4}[\\dA-Z]{13}",
      "LB": "\\d{4}[\\dA-Z]{20}",
      "LI": "\\d{5}[\\dA-Z]{12}",
      "LT": "\\d{16}",
      "LU": "\\d{3}[\\dA-Z]{13}",
      "MK": "\\d{3}[\\dA-Z]{10}\\d{2}",
      "MT": "[A-Z]{4}\\d{5}[\\dA-Z]{18}",
      "MR": "\\d{23}",
      "MU": "[A-Z]{4}\\d{19}[A-Z]{3}",
      "MC": "\\d{10}[\\dA-Z]{11}\\d{2}",
      "MD": "[\\dA-Z]{2}\\d{18}",
      "ME": "\\d{18}",
      "NL": "[A-Z]{4}\\d{10}",
      "NO": "\\d{11}",
      "PK": "[\\dA-Z]{4}\\d{16}",
      "PS": "[\\dA-Z]{4}\\d{21}",
      "PL": "\\d{24}",
      "PT": "\\d{21}",
      "RO": "[A-Z]{4}[\\dA-Z]{16}",
      "SM": "[A-Z]\\d{10}[\\dA-Z]{12}",
      "SA": "\\d{2}[\\dA-Z]{18}",
      "RS": "\\d{18}",
      "SK": "\\d{20}",
      "SI": "\\d{15}",
      "ES": "\\d{20}",
      "SE": "\\d{20}",
      "CH": "\\d{5}[\\dA-Z]{12}",
      "TN": "\\d{20}",
      "TR": "\\d{5}[\\dA-Z]{17}",
      "AE": "\\d{3}\\d{16}",
      "GB": "[A-Z]{4}\\d{14}",
      "VG": "[\\dA-Z]{4}\\d{16}"
    };

    bbanpattern = bbancountrypatterns[ countrycode ];

    // As new countries will start using IBAN in the
    // future, we only check if the countrycode is known.
    // This prevents false negatives, while almost all
    // false positives introduced by this, will be caught
    // by the checksum validation below anyway.
    // Strict checking should return FALSE for unknown
    // countries.
    if ( typeof bbanpattern !== "undefined" ) {
      ibanregexp = new RegExp( "^[A-Z]{2}\\d{2}" + bbanpattern + "$", "" );
      if ( !( ibanregexp.test( iban ) ) ) {
        error_string = "ungültiges länderspezifisches Format";
        is_iban_valid = false;
      }
    }
    if (is_iban_valid) {
      // Now check the checksum, first convert to digits
      ibancheck = iban.substring( 4, iban.length ) + iban.substring( 0, 4 );
      for ( i = 0; i < ibancheck.length; i++ ) {
        charAt = ibancheck.charAt( i );
        if ( charAt !== "0" ) {
          leadingZeroes = false;
        }
        if ( !leadingZeroes ) {
          ibancheckdigits += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf( charAt );
        }
      }
    //  Calculate the result of: ibancheckdigits % 97
      for ( p = 0; p < ibancheckdigits.length; p++ ) {
        cChar = ibancheckdigits.charAt( p );
        cOperator = "" + cRest + "" + cChar;
        cRest = cOperator % 97;
      }
      if ( ! (cRest === 1) ) {
        error_string = "Prüfziffer stimmt nicht";
        is_iban_valid = false;
      }    
    }
  }
  if (!is_iban_valid) {
    $("#edit-iban-valid").html("<div class=\"error-message\"><label>&nbsp;</label>" + error_string + "</div>");
    $(".form-item-iban-valid").show();
  } else {
    $(".form-item-iban-valid").hide();
  }
  if (iban.startsWith("DE")) {
    input_labels_ausland.removeClass('form-required');
    inputs_ausland.attr('required', false);
    inputs_ausland.attr('placeholder', 'Nur für Ausland erforderlich');
  } else {
    input_labels_ausland.addClass('form-required');
    inputs_ausland.attr('required', true);
    inputs_ausland.attr('placeholder', '');
  }
})

});

})(jQuery);
