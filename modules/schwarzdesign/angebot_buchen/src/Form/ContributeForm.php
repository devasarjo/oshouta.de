<?php
/**
 * @file
 * Contains \Drupal\angebot_buchen\Form\ContributeForm.
 */
namespace Drupal\angebot_buchen\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use \Drupal\node\Entity\Node;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\webform\Utility\WebformOptionsHelper;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Url;
/**
 * Contribute form.
 */

class ContributeForm extends FormBase {


  public function getFormId() {
    return 'angebot_buchen';
  }


  public function angebot_buchen_redirect_handler($form, FormStateInterface $form_state) {
    // Force redirect to "/sent"
    //$dest_url = "/node/204";
    //$url = Url::fromUri('internal:' . $dest_url);
    //$form_state->setRedirectUrl( $url );
  }


  public function checkNode(NodeInterface $node = NULL){

    if($node->getType() !== 'angebot'){
      return false;
    } else{
      return true;
    }
  }



  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {

// echo "<br />node title ... " . $node->get('title')->value;
// echo "<br />node type ... " . $node->getType();
// echo "<br />node id ... " . $node->id();


// $fieldStorageZahlungsweise = \Drupal\field\Entity\FieldStorageConfig::loadByName('node', 'field_zahlungsweise');
// $zahlungsweise_options = $fieldStorageZahlungsweise->getSetting('allowed_values');

$fieldStorageGeschlecht = \Drupal\field\Entity\FieldStorageConfig::loadByName('node', 'field_geschlecht');
$geschlecht_options = $fieldStorageGeschlecht->getSetting('allowed_values');

$currentLanguageCode = \Drupal::languageManager()->getCurrentLanguage()->getId();
$mycountries = array();
$country_options = CountryManager::getStandardList();
//$country_options = array_combine($mycountries, $mycountries);


$fieldStoragePreistarife = $this->getPreistarife($node->id());
$arr_tarif_counter = count($fieldStoragePreistarife);

 //die('<br />Test schwarzdesign');

if($this->checkNode($node)){
$form['#cache'] = ['max-age' => 0];

$form['kontaktdaten'] = array(
  '#type' => 'fieldset',
  '#title' => $this->t('1.Kontaktdaten'),
);

$form['sonstiges'] = array(
  '#type' => 'fieldset',
  '#title' => $this->t('2.Sonstiges'),
);

$form['preis_bezahlung'] = array(
  '#type' => 'fieldset',
  '#title' => $this->t('3.Preis & Bezahlung'),
);

$form['mitteilungen'] = array(
  '#type' => 'fieldset',
  '#title' => $this->t('4.Mitteilungen'),
);

// hidden Fields
    $form['seminar'] = array(
        '#type' => 'hidden',
        '#value' => $node->id(),
        '#attributes' => array('readonly' => 'readonly'),
    );
    $form['language'] = array(
        '#type' => 'hidden',
        '#value' => $currentLanguageCode,
        '#attributes' => array('readonly' => 'readonly'),
    );
    $form['seminarname'] = array(
        '#type' => 'hidden',
        '#title' => t('Seminar'),
        '#value' => $node->get('title')->value,
        '#attributes' => array('readonly' => 'readonly'),
    );
    $form['mandatsnr'] = array(
        '#type' => 'hidden',
        '#value' => substr(uniqid('institut'), -6),
        '#attributes' => array('readonly' => 'readonly'),
    );

// 1.Kontaktdaten
  $form['kontaktdaten']['geschlecht'] = array(
    '#type' => 'select',
    '#title' => t('Anrede'),
    '#options' => $geschlecht_options,
    '#required' => TRUE,
  );

    $form['kontaktdaten']['vorname'] = array(
      '#type' => 'textfield',
      '#title' => t('Vorname'),
      '#required' => TRUE,
    );
    $form['kontaktdaten']['nachname'] = array(
      '#type' => 'textfield',
      '#title' => t('Nachname'),
      '#required' => TRUE,
    );
    $form['kontaktdaten']['firma'] = array(
      '#type' => 'textfield',
      '#title' => t('Firma'),
    );
    $form['kontaktdaten']['strasse'] = array(
      '#type' => 'textfield',
      '#title' => t('Straße & Hausnummer'),
      '#required' => TRUE,
    );
    $form['kontaktdaten']['plz'] = array(
      '#type' => 'textfield',
      '#title' => t('PLZ & Ort'),
      '#required' => TRUE,
    );
    $form['kontaktdaten']['ort'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
    );
    $form['kontaktdaten']['land'] = array(
      '#type' => 'select',
      '#title' => t('Land'),
      '#options' => $country_options,
      '#required' => TRUE,
      '#default_value' => 'DE',
    );
    $form['kontaktdaten']['telefon'] = array(
      '#type' => 'tel',
      '#title' => t('Telefon'),
      '#required' => TRUE,
    );
    $form['kontaktdaten']['email'] = array(
      '#type' => 'email',
      '#title' => t('E-Mail'),
      '#required' => TRUE,
    );

// 2.Sonstiges
  $form['sonstiges']['uebernachtung'] = array(
    '#type' => 'checkbox',
    '#title' => t('Übernachtung im Seminarraum gewünscht<br/>(wenn die CoronaSchVO es wieder zulässt).<br/>&nbsp;'),
    '#default_value' => '0',
  );

  $form['sonstiges']['uebernachtungsinfo'] = array(
    '#markup' => '<div class="uebernachtungsinfo">'.$this->t('Infotext Übernachtung').'</div>',
  );

// 3.Preis + Bezahlung
// echo "<br />arr_tarif_counter: " . $arr_tarif_counter;
if($arr_tarif_counter > 0)
{

  $form['preis_bezahlung']['preis_tarif'] = array(
    '#type' => 'select',
    '#title' => t('Preistarife'),
    '#description' => t('Wähle den passenden Tarif aus.'),
    '#options' => $fieldStoragePreistarife,
  );

}
else {
  $form['preis_bezahlung']['preis_tarif'] = array(
    '#type' => 'textfield',
    '#title' => t('Preis/Tarif'),
    '#attributes' => array('readonly' => 'readonly'),
    '#placeholder' => $node->get('field_preis')->value.' €',
    '#default_value' => $node->get('field_preis')->value.' €',
    '#value' => $node->get('field_preis')->value.' €',
  );
}




  $form['preis_bezahlung']['zahlungsweise'] = array(
    '#type' => 'select',
    '#title' => t('Zahlungsweise'),
    '#options' => array(
        '0' => t('Abbuchung Gesamtbetrag'),
        '1' => t('Abbuchung Anzahlung sofort und Restbetrag eine Woche vor Seminarbeginn'),
        '2' => t('Abbuchung nur Anzahlung und Zahlung des Restbetrags mit Ratenvertrag'),
        '3' => t('Überweisung Gesamtbetrag'),
        '4' => t('Überweisung Anzahlung sofort und Restbetrag eine Woche vor Seminarbeginn'),
        '5' => t('Überweisung nur Anzahlung und Zahlung des Restbetrags mit Ratenvertrag')
    ),
    // '#options' => $zahlungsweise_options,
    '#required' => TRUE,
  );
  $form['preis_bezahlung']['kontoinhaber'] = array(
    '#type' => 'textfield',
    '#title' => t('Kontoinhaber'),
    '#required' => FALSE,
  );
  $form['preis_bezahlung']['iban'] = array(
    '#type' => 'textfield',
    '#title' => t('IBAN'),
    '#required' => FALSE,
  );
  $form['preis_bezahlung']['iban_valid'] = array(
    '#type' => 'item',
    '#title' => t('Fehlermeldung'),
    '#markup' => t(''),
    '#required' => FALSE,
  );
  $form['preis_bezahlung']['bic'] = array(
    '#type' => 'textfield',
    '#title' => t('BIC'),
    '#required' => FALSE,
    '#attributes' => array(
      'placeholder' => t('Nur für Ausland erforderlich'),
    ),
  );
  $form['preis_bezahlung']['name_der_bank'] = array(
    '#type' => 'textfield',
    '#title' => t('Name der Bank'),
    '#required' => FALSE,
    '#attributes' => array(
      'placeholder' => t('Nur für Ausland erforderlich'),
    ),
  );

// 4.Mitteilungen
    $form['mitteilungen']['sonstige_mitteilungen'] = array(
      '#type' => 'textarea',
      '#title' => t('Sonstige Mitteilungen'),
    );

    // Angebot ID
    $form['angebot_id'] = array(
      '#type' => 'hidden',
      '#value' => $node->id(),
      '#attributes' => array('readonly' => 'readonly'),
    );
    // Captcha
    // $form['captcha'] = array(
    //   '#type' => 'captcha',
    //   '#captcha_type' => 'recaptcha/reCAPTCHA',
    // );

    // Captcha Image
    $form['captcha'] = array(
      '#type' => 'captcha',
      '#captcha_type' => 'image_captcha/Image',
    );

    // Addes the honeypot protection
    honeypot_add_form_protection($form, $form_state, array('honeypot', 'time_restriction'));
    
    // AGB, Submit
    $form['agb'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ich erkenne die <a href="/agb">Allgemeinen Geschäftsbedingungen</a> an.'),
      '#required' => TRUE,
      '#default_value' => '0',
    );

    $form['required_fields'] = array(
      '#markup' => $this->t('<div id="edit-required-fields"><span>*</span> - Diese Angaben sind erforderlich.</div>'),
    );



    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#default_value' => $this->t('Jetzt online buchen'),
          '#button_type' => 'primary',
        );


// foreach($form['actions']['submit'] as $key => $val)
// {
//     echo "<br />key: " . $key;
// }

    return $form;
  } else{
    return $this->redirect('<front>');
  }

  }


  /**
 * get Preistarife / statt 1 Preis
 *
 * @return array
 */
private function getPreistarife($current_nid) {

// echo "<br />current nid: " . $current_nid;
$tarife_results = array();

$result = db_query("
SELECT t.field_preistarife_target_id as tarif_id,
tlabel.field_tarifbezeichnung_value as tarif_label,
tval.field_tarifpreis_value as tarif_value
FROM node__field_preistarife as t
LEFT JOIN paragraph__field_tarifbezeichnung as tlabel
ON (tlabel.entity_id = t.field_preistarife_target_id
AND tlabel.deleted = 0)
LEFT JOIN paragraph__field_tarifpreis as tval
ON (tval.entity_id = t.field_preistarife_target_id
AND tval.deleted = 0)
WHERE t.entity_id = ".$current_nid."
AND t.deleted = 0
ORDER BY t.delta ASC;
");

$counter = 0;

while($row = $result->fetchAssoc()){

// echo "<br />tarif_id: " . $row['tarif_id'];
// echo "<br />tarif_label: " . $row['tarif_label'];
// echo "<br />tarif_value: " . $row['tarif_value'];
//
if($row['tarif_label'] != "" AND $row['tarif_value'] != "") {
  $tarife_results[$row['tarif_label'] .', '. $row['tarif_value'].' €'] = $row['tarif_label'] .', '. $row['tarif_value'].' €';
}


$counter ++;

}

return $tarife_results;

}

  // public function validateForm(array &$form, FormStateInterface $form_state) {
  //
  //
  // }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    // foreach ($form_state->getValues() as $key => $value) {
    //   drupal_set_message($key . ': ' . $value);
    // }

    // Email versenden
    $form_values = $form_state->getValues();
    //$params = $form_values;

    $params = $form_values;

    $module = 'angebot_buchen';
    $keys = array('send_mail_client','send_mail_uta');


  foreach($keys as $key){

      if($key == 'send_mail_client'){
        $to = $form_values['email'];
        $from = 'Osho UTA Institut <buchung@oshouta.info>';
      } else{
        //$to = 'katharina.feldkamp@uta-cologne.de';
        //$to = 'merle@preview-server.de';
        $to = 'buchung@oshouta.info';
        $from = 'Osho UTA Institut <buchung@oshouta.info>';
      }

      //$language_code = $this->languageManager->getDefaultLanguage()->getId();
      $language_code = 'de';
      $send_now = TRUE;


      $result = \Drupal::service('plugin.manager.mail')->mail($module, $key, $to, $language_code, $params, $from, $send_now);
     }

    if ($result['result'] == TRUE) {
      // $firstname = $params['vorname'];
      // $surname = $params['nachname'];
      // $email = $params['email'];
      // $response = Url::fromUserInput("/newsletter-zustimmung-zur-kontaktaufnahme?edit[firstname][und][0][value]=$firstname&edit[surname][und][0][value]=$surname&edit[email][und][0][value]=$email");
      // $form_state->setRedirectUrl($response);

      $form_state->setRedirect(
        'entity.node.canonical',
        ['node' => $params['seminar']]
      );
      drupal_set_message(t('Deine Angaben wurden abgesendet. Vielen Dank für Deine Buchung.'));
    } else {
      drupal_set_message(t('Es ist ein Fehler aufgetreten. Bitte überprüfe Deine Angaben.'), 'error');
    }

    //Bestellung Node erstellen
    // if($result['result'] == TRUE){
      $node = Node::create([
        'type' => 'anmeldung',
        'title' => 'Anmeldung: '.$params['seminarname'].' von '.$params['vorname'].' '.$params['nachname'],
        'field_seminar_ref' => $params['seminar'],
        'field_geschlecht' => $params['geschlecht'],
        'field_vorname' => $params['vorname'],
        'field_nachname' => $params['nachname'],
        'field_firma' => $params['firma'],
        'field_strasse' => $params['strasse'],
        'field_plz' => $params['plz'],
        'field_ort' => $params['ort'],
        'field_land' => $params['land'],
        'field_telefon' => $params['telefon'],
        'field_e_mail' => $params['email'],
        'field_uebernachtung' => $params['uebernachtung'],
        'field_preis_tarif' => $params['preis_tarif'],
        'field_zahlungsweise' => $params['zahlungsweise'],
        'field_kontoinhaber' => $params['kontoinhaber'],
        'field_iban' => $params['iban'],
        'field_bic' => $params['bic'],
        'field_name_der_bank' => $params['name_der_bank'],
        'field_sonstige_mitteilungen' => $params['sonstige_mitteilungen']
      ]);
      $node->save();
    // }
  }


}
?>
