<?php

  /**
   * @file
   * Definition of Drupal\uta_datum_filter\Plugin\views\filter\UtaDatumFilter.
   */

  namespace Drupal\uta_datum_filter\Plugin\views\filter;

  use Drupal\views\Plugin\views\filter\FilterPluginBase;
  use Drupal\views\Plugin\views\filter\InOperator;
  use Drupal\views\Plugin\views\filter\ManyToOne;
  use Drupal\views\ViewExecutable;
  use Drupal\views\Views;
  /**
   * Filters by given list of node title options.
   *
   * @ingroup views_filter_handlers
   *
   * @ViewsFilter("uta_datum_filter")
   */
  class UtaDatumFilter extends FilterPluginBase {
      // exposed filter options
      protected $alwaysMultiple = TRUE;

      /**
       * Provide simple equality operator
       */
      public function operatorOptions() {
          return [
              'online' => $this->t('Online'),
              'upcoming' => $this->t('Upcoming'),
              'upcoming_online' => $this->t('Upcoming und Online'),
              'archive' => $this->t('Vergangene'),
              'upcoming_queriable' => $this->t('Upcoming + Public URL Query'), // adds support for filtering through GET parameters
              'upcoming_or_empty' => $this->t('Upcoming, Online or Empty')
          ];
      }


      public function query() {
          //Get the current domain.
          //$domain = domain_get_domain();
          $nu_in_utc = new \DateTime('now', new \DateTimezone('UTC'));
          $nu_in_utc_in_iso = $nu_in_utc->format('Y-m-d\TH:i:s');
          $nu_date = $nu_in_utc->format('Y-m-d');


          $configuration = [
              'table'      => 'node__field_zeitraum',
              'left_table' => 'node_field_data',
              'left_field' => 'nid',
              'field'      => 'entity_id',
              'type'       => 'LEFT',
              'extra_operator'   => 'AND',
          ];
          $join = Views::pluginManager('join')->createInstance('standard', $configuration);
          $this->query->addRelationship('node__field_zeitraum', $join, 'node_field_data');


          // evaluates date from get parameter
          $query_date_isset = false;
          if($this->operator == 'upcoming_queriable'){
            if(isset($_GET['datum']) && !empty($_GET['datum'])){
              if($query_timestamp = strtotime($_GET['datum'])){
                $query_timestamp = '@' . $query_timestamp;
                $query_date = new \DateTime($query_timestamp);
                $query_nu_in_utc_in_iso = $query_date->format('Y-m-d\TH:i:s');
                $query_nu_date = $query_date->format('Y-m-d');
                $query_date_isset = true;
              }
            }


          }



          switch($this->operator) {
              case 'online':
                  /*
                  * Condition 'Online'
                  */
                  $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_end_value', $nu_in_utc_in_iso, '>');
                  $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_value', $nu_date, '<=');
                  break;
              case 'upcoming':
                  /*
                  * Condition 'Upcoming'
                  */
                  $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_end_value', $nu_in_utc_in_iso, '>');
                  $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_value', $nu_date, '>');
                  break;

              case 'upcoming_online':
                  /*
                  * Condition 'Upcoming and Online'
                  */
                  $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_end_value', $nu_in_utc_in_iso, '>=');
                  break;

              case 'archive':
                  /*
                  * Condition 'Archive'
                  */
                  $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_end_value', $nu_in_utc_in_iso, '<');
                  break;

              case 'upcoming_queriable':
                  /*
                  * Condition 'Upcoming + Public URL Query'
                  */
                  $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_end_value', $nu_in_utc_in_iso, '>');
                  $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_value', $nu_date, '>=');

                  if($query_date_isset){
                    $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_end_value', $query_nu_in_utc_in_iso, '>');
                    $this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_value', $query_nu_date, '>=');
                  }
                  break;

              case 'upcoming_or_empty':
                    /*
                    * Condition 'Upcoming, Online or Empty'
                    */
                    //$this->query->addWhere('AND', 'node__field_zeitraum.field_zeitraum_end_value', $nu_in_utc_in_iso, '>=');
                    $this->query->addWhere(
                      $this->options['group'],
                        db_or()
                          ->condition('node__field_zeitraum.field_zeitraum_end_value', $nu_in_utc_in_iso, '>=')
                          ->condition('node__field_zeitraum.field_zeitraum_end_value', $nu_in_utc_in_iso, 'IS NULL')
                        );
                    break;


          }
      }
  }
