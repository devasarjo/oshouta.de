<?php

/**
 * @file
 * Contains \Drupal\angebot_next\Plugin\Block\AngbotNextBlock.
 */

namespace Drupal\angebot_next\Plugin\Block;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatch;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;
use Drupal\Core\Cache\Cache;

/**
 * Provides AngebotNextBlock.
 *
 * @Block(
 *   id="angebot_next",
 *   admin_label = @Translation("Angebot Next"),
 * )
 */
class AngebotNextBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {


  $build = array(
    '#cache' => array(
      'tags' => $this->getCacheTags(),
      'contexts' => $this->getCacheContexts(),
      'max-age' => $this->getCacheMaxAge()
    )
   );

  $angebot_content = $this->generateNextAngebote();
  $build['#data'] = $angebot_content;

  $language = \Drupal::languageManager()->getCurrentLanguage();
  $current_langcode = $language->getId();
  
  $tafel_text = "";
  $tafel_text_trans = $this->getTafelText($current_langcode);
  if($tafel_text_trans != "") {
    $tafel_text = '<div class="row tafel_text"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'.$tafel_text_trans.'</div></div>';
  }

  $build['#tafel'] = $tafel_text;

	$build['#theme'] = 'angebot_next';

  return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user.node_grants:view']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['node_list']);
  }

/**
* {@inheritdoc}
*/
public function getCacheMaxAge() {
  return 0;
}


/**
 * Next Angebote Liste
 *
 * @return array
 */
private function generateNextAngebote() {

//today
  $nu_in_utc = new \DateTime('now', new \DateTimezone('UTC'));
  $query_today_in_iso = $nu_in_utc->format('Y-m-d\TH:i:s');
  $query_today = $nu_in_utc->format('Y-m-d');
  $day_today = $nu_in_utc->format('d.m.Y');

//in 3 days
  $nu_in_utc_in_3 = new \DateTime('now', new \DateTimezone('UTC'));
  $nu_in_utc_in_3->modify('+3 day');
  $query_today_and_3 = $nu_in_utc_in_3->format('Y-m-d');   //  in 3 Tagen

//tomorrow / 1 day
  $nu_in_utc_in_1 = new \DateTime('now', new \DateTimezone('UTC'));
  $nu_in_utc_in_1->modify('+1 day');
  $day_tomorrow = $nu_in_utc_in_1->format('d.m.Y');   //  in 1 Tag

//in 3 days
  $nu_in_utc_in_2 = new \DateTime('now', new \DateTimezone('UTC'));
  $nu_in_utc_in_2->modify('+2 day');
  $day_today_and_2 = $nu_in_utc_in_2->format('d.m.Y');   //  in 2 Tagen

  // $day_today = date('d.m.Y');         //  heute
  // $day_tomorrow = date('d.m.Y', strtotime("+1 day"));      //  morgen
  // $day_today_and_2 = date('d.m.Y', strtotime("+2 day"));   //  uebermorgen
  //


$kurzmassagenkalender_type = 45;

$test_date = new DrupalDateTime();
$result = array();
$my_results = array();
$counter = 0;
//echo "<br />a_datum: " . $a_datum;

$language = \Drupal::languageManager()->getCurrentLanguage();
$current_langcode = $language->getId();

$tmp_subheadline = '';
$tmp_flag_today = 0;
$tmp_flag_tomorrow = 0;
$tmp_flag_today_and_2 = 0;

if(date('I')) {
  $sql_order = "ORDER BY value.the_sub_summer";
  // echo "<br />Sommerzeit";
} else {
  $sql_order = "ORDER BY value.the_sub_winter";
  // echo "<br />Winterzeit";
}

$result = db_query("SELECT DISTINCT value.node_id as 'Node_ID', value.date_start as 'Start_Date', value.date_end as 'End_Date', value.termintyp, value.title, value.the_sub_summer, value.the_sub_winter

FROM
(
  (SELECT n.nid as node_id, fdu.field_datum_uhrzeit_value as date_start, fdu.field_datum_uhrzeit_end_value as date_end, 0 as termintyp,
    n.title,
    DATE_ADD(fdu.field_datum_uhrzeit_value, INTERVAL 1 HOUR) as the_sub_winter,
    DATE_ADD(fdu.field_datum_uhrzeit_value, INTERVAL 2 HOUR) as the_sub_summer
  FROM node__field_datum_uhrzeit as fdu
  INNER JOIN node__field_termin as ft
  ON (ft.field_termin_target_id = fdu.entity_id)
  INNER JOIN node_field_data as n
  ON (n.nid = ft.entity_id AND n.status = 1)
  )
  UNION
(
  SELECT n.nid as node_id, ftr.field_termin_regel_value_raw as date_start, ftr.field_termin_regel_end_value_raw as date_end, 1 as termintyp,
  n.title,
  ftr.field_termin_regel_value_raw as the_sub_winter,
  ftr.field_termin_regel_value_raw as the_sub_summer
  FROM date_recur__node__field_termin_regel as ftr
  INNER JOIN node_field_data as n
  ON (n.nid = ftr.entity_id AND n.status = 1)
  INNER JOIN node__field_termin_regel as tr
  ON (tr.entity_id = ftr.entity_id AND tr.deleted = 0)
)
) value


LEFT JOIN node__field_thema as thema
ON (thema.entity_id = value.node_id)
LEFT JOIN node__field_typ as typ
ON (typ.entity_id = value.node_id)

WHERE value.date_start >= '".$query_today."'
AND value.date_start < '".$query_today_and_3."'
" . $sql_order);




// echo "<br />SELECT DISTINCT value.node_id as 'Node_ID', value.date_start as 'Start_Date', value.date_end as 'End_Date', value.termintyp, value.title, value.the_sub_summer, value.the_sub_winter
//
// FROM
// (
//   (SELECT n.nid as node_id, fdu.field_datum_uhrzeit_value as date_start, fdu.field_datum_uhrzeit_end_value as date_end, 0 as termintyp,
//     n.title,
//     DATE_ADD(fdu.field_datum_uhrzeit_value, INTERVAL 1 HOUR) as the_sub_winter,
//     DATE_ADD(fdu.field_datum_uhrzeit_value, INTERVAL 2 HOUR) as the_sub_summer
//   FROM node__field_datum_uhrzeit as fdu
//   INNER JOIN node__field_termin as ft
//   ON (ft.field_termin_target_id = fdu.entity_id)
//   INNER JOIN node_field_data as n
//   ON (n.nid = ft.entity_id AND n.status = 1)
//   )
//   UNION
// (
//   SELECT n.nid as node_id, ftr.field_termin_regel_value_raw as date_start, ftr.field_termin_regel_end_value_raw as date_end, 1 as termintyp,
//   n.title,
//   ftr.field_termin_regel_value_raw as the_sub_winter,
//   ftr.field_termin_regel_value_raw as the_sub_summer
//   FROM date_recur__node__field_termin_regel as ftr
//   INNER JOIN node_field_data as n
//   ON (n.nid = ftr.entity_id AND n.status = 1)
//   INNER JOIN node__field_termin_regel as tr
//   ON (tr.entity_id = ftr.entity_id AND tr.deleted = 0)
// )
// ) value
//
//
// LEFT JOIN node__field_thema as thema
// ON (thema.entity_id = value.node_id)
// LEFT JOIN node__field_typ as typ
// ON (typ.entity_id = value.node_id)
//
// WHERE value.date_start >= '".$query_today."'
// AND value.date_start < '".$query_today_and_3."'
// " . $sql_order;


while($row = $result->fetchAssoc()){


  $entity_manager = \Drupal::entityTypeManager();
  $node = $entity_manager->getStorage('node')->load($row['Node_ID']);
  $typ_tid = $node->get('field_typ');

  $typ_id = $typ_tid[0]->target_id;

  $tmp_title = $node->get('title')->value;

  if($kurzmassagenkalender_type == $typ_id) {
    $tmp_path_alias = "/".$current_langcode."/kurzmassagen";
  }
  else {
    $tmp_path_alias = "/".$current_langcode.\Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$row['Node_ID']);
  }

  $dozenten = $node->get('field_dozenten');
  $counter_dozent = 0;
  $tmp_doz_string = '';
  $tmp_doz_array = array();
  foreach ($dozenten as $dozenten_ids) {

    $dozenten_uid = get_profile_dozent_from_user($dozenten_ids->target_id);
    $dozenten_name = get_name_from_user($dozenten_ids->target_id);
    $tmp_doz_array[$counter] = '<a href="/'.$current_langcode.'/profile/'.$dozenten_uid['profile_id'].'">'.$dozenten_name.'</a>';

    if($counter_dozent == 0) {
      $tmp_doz_string .= $tmp_doz_array[$counter];
    } else {
      $tmp_doz_string .= ','.$tmp_doz_array[$counter];
    }

    $counter_dozent ++;

  }

  $tmp_start = $row['Start_Date'];
  $tmp_ende = $row['End_Date'];



date_default_timezone_set('UTC');
$tmp_start_timestamp = strtotime($tmp_start);
$tmp_ende_timestamp = strtotime($tmp_ende);

//$tmp_date_formatted = $this->formatterTerminString($tmp_start_timestamp,$tmp_ende_timestamp);
$tmp_date_formatted = $this->formatterTerminTimeString($tmp_start_timestamp,$row['termintyp']);
//echo "<br />tmp_date_formatted: " . $tmp_date_formatted;


$is_start_date = \Drupal::service('date.formatter')->format($tmp_start_timestamp, 'uta_date_short');

if(($is_start_date == $day_today) AND ($tmp_flag_today == 0)) {
  $day_desc = "Heute";
  if ($current_langcode == 'en') {
    $day_desc = "Today";
  }
  $tmp_subheadline = '<h4 class="no_margin_bottom">'.$day_desc.'</h4>';
  $tmp_flag_today = 1;
} elseif(($is_start_date == $day_tomorrow) AND ($tmp_flag_tomorrow == 0)) {
  $day_desc = "Morgen";
  if ($current_langcode == 'en') {
    $day_desc = "Tomorrow";
  }
  $tmp_subheadline = '<h4 class="padding_top no_margin_bottom">'.$day_desc.'</h4>';
  $tmp_flag_tomorrow = 1;
} elseif(($is_start_date == $day_today_and_2) AND ($tmp_flag_today_and_2 == 0)) {
  $day_desc = "Übermorgen";
  if ($current_langcode == 'en') {
    $day_desc = "Day after tomorrow";
  }  
  $tmp_subheadline = '<h4 class="padding_top no_margin_bottom">'.$day_desc.'</h4>';
  $tmp_flag_today_and_2 = 1;
} else {
  $tmp_subheadline = "";
}


  $my_results[$counter]['subheadline'] = $tmp_subheadline;
  $my_results[$counter]['nid'] = $row['Node_ID'];
  $my_results[$counter]['zeitraum'] = $tmp_date_formatted;
  $my_results[$counter]['title'] = $tmp_title;
  $my_results[$counter]['path_alias'] = $tmp_path_alias;
  $my_results[$counter]['dozenten'] = $tmp_doz_string;


  $counter ++;
}

return $my_results;

}



/**
 * Formatter for Termin Dates
 *
 * @return string  DateString
 */

 private function formatterTerminString($utc_termin_start_timestamp,$utc_termin_ende_timestamp,$termintyp) {

  date_default_timezone_set('Europe/Berlin');
  $date_formatted = '';

  $start_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_short');
  $ende_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_short');

  if($start_formatted_short != $ende_formatted_short) {
    $date_formatted = $start_formatted_short . ' - ' . $ende_formatted_short;
  }
  else {
    $start_formatted = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_medium_date');
    $ende_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_time');
    $date_formatted = $start_formatted . ' - ' . $ende_formatted_time . ' Uhr';
  }


  return $date_formatted;
}


/**
 * Formatter for Termin Dates
 *
 * @return string  DateString
 */

 private function formatterTerminTimeString($utc_termin_start_timestamp,$termintyp) {



if($termintyp == 1) {
  date_default_timezone_set('UTC');
}
else {
  date_default_timezone_set('Europe/Berlin');
}

//date_default_timezone_set('UTC');

  $date_formatted = '';

  $date_formatted = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_time') . ' Uhr';

  return $date_formatted;
}


/**
 * get Text Tafel Startseite
 *
 * @return string  TafelText
 */

 private function getTafelText($langcode) {

  $tafel_text = '';

  $result_tafel = db_query("
    SELECT field_tafel_aktuelles_value as tafel
    FROM node__field_tafel_aktuelles
    WHERE deleted = 0 AND langcode = '"
    . $langcode . "' AND bundle = 'startseite'
    LIMIT 1;
  ");

  while($row_tafel = $result_tafel->fetchAssoc()){

    $tafel_text = $row_tafel['tafel'];

  }

  return $tafel_text;
}





}
